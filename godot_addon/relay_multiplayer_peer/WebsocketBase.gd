extends Node

class_name WebsocketBase

@export var url = "ws://localhost:7788/relays"

var socket = WebSocketPeer.new()
var current_state = WebSocketPeer.STATE_CLOSED

signal state_changed(state)
signal opened()
signal closed(code)
signal message_received(bytes)

func _ready():
	pass

func connect_socket():	
	var err = socket.connect_to_url(url)
	if err != OK:
		print("Unable to connect")
		set_process(false)
	else:
		current_state = WebSocketPeer.STATE_CONNECTING
	return err

func _handle_state():
	var state = socket.get_ready_state()
	if state != current_state:
		#print("state changed: " + str(state))
		emit_signal(state_changed.get_name(), state)
		current_state = state
		match current_state:
			WebSocketPeer.STATE_OPEN:
				emit_signal(opened.get_name())
			WebSocketPeer.STATE_CLOSED:
				var code = socket.get_close_code()
				emit_signal(closed.get_name(), code)

func poll():
	#print("poll")
	socket.poll()
	_handle_state()
	if current_state == WebSocketPeer.STATE_OPEN:
		while socket.get_available_packet_count():
			emit_signal(message_received.get_name(), socket.get_packet())

func _process(_delta):
	poll()

func close():
	socket.close()
