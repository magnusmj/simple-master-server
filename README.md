# Simple Master Server

This project is to enable:

- matchmaking and server discovery.
- client hosting through websocket relay.

```gdscript
var server = RelayMultiplayerPeer.new()  
server.create_server("wss://" + domain + "/relays", 255, "default-password")
multiplayer.multiplayer_peer = server
```
```gdscript
var client = RelayMultiplayerPeer.new()	
client.create_client("wss://" + domain + "/relays", relay_id, "default-password")
multiplayer.multiplayer_peer = client
```
