"use strict";
import { RelayTypes, MessageTypes } from "./relay-shared.js";

class RelayBase extends EventTarget {
  constructor(url, relayType, password) {
    super(); 
  	this.paddingSize = 4;
    this.ws = new WebSocket(url);
    this.relayId = "";
    this.relaySocket = null;
    this.ws.onopen = () => {
      this.ws.onmessage = (msg) => this.#onMessage(msg);
      this.ws.onclose = () => this.#onClose();
      const connectionData = { 
      	password: password, 
      	relayType: relayType 
      }
      this.ws.send(JSON.stringify(connectionData));
    }
  }
  #onClose(){
  	console.log("onClose");
  }
  async #onMessage(msg) {
  	const data = await msg.data.arrayBuffer();
  	const bytes = new Uint8Array(data);
    switch (bytes[0]) {
      case MessageTypes.MessageTo:	      
        this.#handleMessageTo(bytes);
      	break;
      case MessageTypes.MessageAll:
      	this.#handleMessageAll(bytes);
      	break;
      case MessageTypes.Init:
      	this.#handleInitMessage(bytes.subarray(4, bytes.length));
      	break;
    }
  }
  #handleMessageTo(bytes){       
	  //console.log("handleMessageTo");
    let e = new Event('messageto');
		e.bytes = bytes;    
		e.from = bytes[2];
		e.to = bytes[1];
		e.msg = bytes.subarray(4, bytes.length);
    this.dispatchEvent(e);
  }
  #handleMessageAll(){
	  console.log("handleMessageAll");
  	let e = new Event('messageall');
		e.bytes = bytes;    
		e.from = bytes[2];
		e.to = bytes[1];
		e.msg = bytes.subarray(4, bytes.length);
    this.dispatchEvent(e);
  }
  #handleInitMessage(bytes){
  	//console.log("handleInitMessage");
  	let text = new TextDecoder().decode(bytes);
  	let initMessage = JSON.parse(text);
		this.relayId = initMessage.relayId;
		this.relaySocket = initMessage.relaySocket;
		let e = new Event('init');
		e.initMessage = initMessage;
		this.dispatchEvent(e);
  }
  messageToBytes(message) {
		let originalBuffer = message instanceof ArrayBuffer ? message : new TextEncoder().encode(message).buffer;
		let totalSize = originalBuffer.byteLength + this.paddingSize;
		let paddedBuffer = new ArrayBuffer(totalSize);
		let view = new Uint8Array(paddedBuffer);
		view.set(new Uint8Array(originalBuffer), this.paddingSize); // Copy original data
		return view; 
  }
  sendTo (message, to) {
  	let bytes = this.messageToBytes(message);
  	bytes[0] = MessageTypes.MessageTo;
  	bytes[1] = to;  	
    this.ws.send(bytes);
  }
  sendAll(message){
  	let bytes = this.messageToBytes(message);
  	bytes[0] = MessageTypes.MessageAll;
    this.ws.send(bytes);
  }
  setRelayData(message){
  	let bytes = this.messageToBytes(message);
  	bytes[0] = MessageTypes.SetRelayData;
    this.ws.send(bytes);
  }
}

export default RelayBase;
