"use strict";
import RelayBase from "./relay-base.js";
import { RelayTypes, MessageTypes } from "./relay-shared.js";

const pass = "default-password";
const iddd = "MyGame";
const port = 7788;
//const relay-hosts


class RelayTester extends RelayBase {
	constructor(url, password){
		super(url, RelayTypes.HostClientRelay, password);
		this.addEventListener("messageto", this.onMessageTo);
		this.decoder = new TextDecoder();
	}
	onMessageTo(e){
		console.log("onMessageTo");
		console.log(e);
		console.log(this.decoder.decode(e.msg));
	} 
}

let rt = new RelayTester("ws://localhost:"+port+"/relays", pass);

async function onPostLog() {
  let data = {
    id: iddd,
    password: pass,
    log: { "betne": 56 }
  }
  let options = {
    method: 'POST',
    body: JSON.stringify(data),
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json'
    },
  }
  let response = await fetch('/log/add', options);
  console.log(await response.text());
}

async function onGetLog() {
  let response = await fetch('/log/get?id=' + iddd + '&password=' + pass);

  console.log(await response.text());
}

async function onClearLog(){

}

Set.prototype.popFirst = function () {
  const firstItem = this.values().next().value; // Get the first item
  if (firstItem !== undefined) {
    this.delete(firstItem); // Remove the item from the Set
  }
  return firstItem; // Return the removed item
};

function onCreateRelay(){
	console.log("onCreateRelay");
	
}

function onJoinRelay(){

}
window.onCreateRelay = onCreateRelay;
window.onJoinRelay = onJoinRelay;
window.rt = rt;
