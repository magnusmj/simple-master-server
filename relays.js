"use strict";

import bcrypt from 'bcryptjs';
import { WebSocketServer } from 'ws';
import { Relay } from "./relay.js";
import { RelayTypes, MessageTypes, RelayNodeTypes } from "./public/js/relay-shared.js";

let relays = null;

class Relays{ 
	constructor(server, config){
		this.relays = {}
		this.config = config;
		this.websocketserver = new WebSocketServer({
	    server: server,
	    path: "/relays"
	  });
	  this.onConnection = this.onConnection.bind(this);
	  this.onMessage = this.onMessage.bind(this);
	  this.onEmpty = this.onEmpty.bind(this);
	  this.websocketserver.on("connection", this.onConnection);
	} 
	onConnection(websocket){
		websocket.addEventListener("message", this.onMessage);
	} 
	onEmpty(relay){
		//console.log("removing empty relay: " + relay.id);
		delete this.relays[relay.id];
	}
	onMessage(e){
		const websocket = e.target;
		try {
			const initData = JSON.parse(e.data);
      const password = initData.password;
      const id = initData.id;
      if (bcrypt.compareSync(password, this.config.passHash)){
        websocket.removeEventListener("message", this.onMessage);
        //console.log("id: " + id);
        if (id in this.relays){
          this.relays[id].connectWebsocket(websocket);
        } else {
        	if (initData.nodeType==RelayNodeTypes.Client) {
        		console.log("is client cant create relay")
        		websocket.close();
        		return;
        	}
          let relay = new Relay(websocket, initData); 
          this.relays[relay.id] = relay;
          relay.once("empty", this.onEmpty);
        }
      } else {
        console.log("wrong password");
        let response = {
          info: "access denied"
        }
        websocket.send(JSON.stringify(response));
        websocket.close();
      }
		} catch (e) {
			console.log(e);
			console.log(e.data);
      websocket.close();
		}
	}
	serialize(){
		let data = []
		for (let id in this.relays){
			data.push(this.relays[id].serialize());
		}
		return data;
	} 
}

function init(server, config){
	relays = new Relays(server, config)	
}

function getRelays(){
	return relays;
}

export { getRelays }
export default init
